# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.5.0](https://gitlab.com/memorize_it/memorize/compare/v0.4.0...v0.5.0) (2020-02-24)

**Note:** Version bump only for package @memorize/file-system





# [0.4.0](https://gitlab.com/memorize_it/memorize/compare/v0.3.1...v0.4.0) (2020-02-15)


### Features

* **file-system:** addtags to metadata when loading zettelkasten ([33db0c6](https://gitlab.com/memorize_it/memorize/commit/33db0c62cc6a69d9fb0c310959ca14ac03e0fa4b))





## [0.3.1](https://gitlab.com/memorize_it/memorize/compare/v0.3.0...v0.3.1) (2020-02-08)


### Bug Fixes

* **types:** use @memorize/types as dependency to allow use of enums,maps... on runtime ([58c64b0](https://gitlab.com/memorize_it/memorize/commit/58c64b06160c105ac9073e82de8b4d087883fe68))





# [0.3.0](https://gitlab.com/memorize_it/memorize/compare/v0.2.0...v0.3.0) (2020-02-08)


### Bug Fixes

* **core:** move regeps from types to core. They are used on the runtime !!! XD ([c613128](https://gitlab.com/memorize_it/memorize/commit/c6131288b5c50d6962e8c59ee6f4df03a4d4c60d))


### Features

* **file-system:** add diagram type to zettelkasten visualization ([ca5e736](https://gitlab.com/memorize_it/memorize/commit/ca5e736a0ee61b587896c9c61aa9e74a8e260ca2))
* **file-system:** create svg file with zettelkasten visualization ([3a13c93](https://gitlab.com/memorize_it/memorize/commit/3a13c9349073d444eac024fcd221b19adf56730c))





# [0.2.0](https://gitlab.com/memorize_it/memorize/compare/v0.1.1...v0.2.0) (2020-01-20)


### Features

* **file-system:** add basic file-system operations ([123c5f0](https://gitlab.com/memorize_it/memorize/commit/123c5f091c74352a7f80199abd185863021f3ef2))
* **file-system:** parse zettel from file and zettel to file ([e24a35d](https://gitlab.com/memorize_it/memorize/commit/e24a35df0b1160480021f1e0620b72bf8b85e476))
