import {
  readFileAsync,
  readdirAsync,
  statAsync,
  writeFileAsync,
} from '@memorize/utils';
import { extname } from 'path';

export interface NewFileAsJsonInterface {
  filename: string;
  content?: object;
  path: string;
}

export interface NewFileInterface {
  filename: string;
  content?: string;
  path: string;
  extension: string;
}

export async function writeFile({
  filename,
  path,
  extension,
  content,
}: NewFileInterface): Promise<void> {
  return writeFileAsync(
    `${path}/${filename}.${extension}`,
    content ? content : '',
  );
}

export async function readFile(path: string): Promise<string> {
  const fileContent = await readFileAsync(path);
  return fileContent.toString('utf8');
}

export async function readFromJson<T>(path: string): Promise<T> {
  return JSON.parse(await readFile(`${path}.json`));
}

export async function writeAsJson({
  filename,
  path,
  content,
}: NewFileAsJsonInterface): Promise<void> {
  return writeFile({
    content: JSON.stringify(content, null, '\t'),
    extension: 'json',
    filename,
    path,
  });
}

export async function searchFilesInDirectory(
  path: string,
  extension: string,
  updatedAfterMs?: number,
): Promise<string[]> {
  const dir = await readdirAsync(path, {
    withFileTypes: true,
  });

  const mdFiles = await Promise.all(
    dir
      .filter(
        (dirent): boolean =>
          dirent.isFile() && extname(dirent.name) === extension,
      )
      .map((dirent): string => `${path}/${dirent.name}`),
  );

  if (!updatedAfterMs) {
    return mdFiles;
  }

  const updatedFiles = await Promise.all(
    mdFiles.map(
      async (path): Promise<string | undefined> => {
        const { mtimeMs } = await statAsync(path);
        return mtimeMs > updatedAfterMs ? path : undefined;
      },
    ),
  );

  return updatedFiles.filter(Boolean) as string[];
}
