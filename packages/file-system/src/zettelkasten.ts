import {
  ZettelInterface,
  ZettelkastenInterface,
  ZettelkastenVisualizationType,
} from '@memorize/types';
import { getZettelkastenVisualizationSvg } from '@memorize/visualization';
import {
  readFromJson,
  writeAsJson,
  writeFile,
  searchFilesInDirectory,
} from './file-system';
import { parseZettel } from './zettel';
import { consolidateTags } from '@memorize/core';

const zettelkastenFilename = '.zettelkasten';

export async function parseZettelkastenDirectory(
  path: string,
  lastUpdateTime?: number,
): Promise<ZettelInterface[]> {
  const zettels = await searchFilesInDirectory(path, '.md', lastUpdateTime);
  return Promise.all(
    zettels.map(
      (z): Promise<ZettelInterface> => {
        try {
          const zettel = parseZettel(z);
          return zettel;
        } catch {
          throw new Error(
            `file-system(parseZettelkastenDirectory): unable to parse ${z}`,
          );
        }
      },
    ),
  );
}

export async function loadZettelkasten(
  rootPath: string,
): Promise<ZettelkastenInterface> {
  let zettelkasten: ZettelkastenInterface;

  const now = Date.now();

  try {
    zettelkasten = await readFromJson(`${rootPath}/${zettelkastenFilename}`);

    const existingZettelIds = zettelkasten.zettels.map(({ id }) => id);

    const updatedZettels = await parseZettelkastenDirectory(
      rootPath,
      zettelkasten.meta.updatedAt,
    );
    let tags = zettelkasten.meta.tags;

    updatedZettels.forEach((updatedZettel) => {
      const updatedZettelId = updatedZettel.id;
      const existingZettelId = existingZettelIds.findIndex(
        (id) => id === updatedZettelId,
      );

      if (existingZettelId < 0) {
        return zettelkasten.zettels.push(updatedZettel);
      }

      zettelkasten.zettels[existingZettelId] = updatedZettel;
      tags = [
        ...tags,
        ...updatedZettel.tags.filter((h) => tags.indexOf(h) < 0),
      ];
    });

    zettelkasten.meta.tags = tags;
  } catch {
    const zettels = await parseZettelkastenDirectory(rootPath);
    zettelkasten = {
      meta: {
        updatedAt: now,
        tags: consolidateTags(zettels),
      },
      zettels: zettels,
    };
  }

  zettelkasten.meta.updatedAt = now;

  await writeAsJson({
    path: rootPath,
    content: zettelkasten,
    filename: zettelkastenFilename,
  });

  return zettelkasten;
}

export async function createZettelkastenSvg(
  path: string,
  type: ZettelkastenVisualizationType,
): Promise<void> {
  const zettelkasten = await loadZettelkasten(path);
  return writeFile({
    filename: 'zettelkasten',
    extension: 'svg',
    path,
    content: await getZettelkastenVisualizationSvg(zettelkasten, type),
  });
}
