import { ZettelInterface, ZettelBodyInterface } from '@memorize/types';
import { writeFile, readFile } from './file-system';
import { lexer, Tokens } from 'marked';
import { basename } from 'path';
import { dump, load } from 'js-yaml';
import dedent from 'dedent';
import { IDRegexp, TagRegexp } from '@memorize/core';

const separator = '---';

function search(regex: RegExp, str: string): string[] {
  const result: string[] = [];
  let m: RegExpExecArray | null;

  while ((m = regex.exec(str)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (m.index === regex.lastIndex) {
      regex.lastIndex++;
    }

    const match = m.pop();

    if (match) {
      result.push(match);
    }
  }
  return result;
}

export function contentToZettel(content: string): ZettelBodyInterface {
  const [, meta, body] = content.split(separator);

  let title = '';

  try {
    const n = (lexer(body).find((z) => z.type === 'heading') as any) as
      | Tokens.Heading
      | undefined;
    title = n ? n.text : '';
  } catch {
    title = '';
  }

  if (!meta) {
    throw new Error(
      `file-system(contentToZettel): unable to parse zettel content, ensure all markdown files respect the format`,
    );
  }

  const metadataInformation = load(meta);
  const references = body
    ? metadataInformation.references.concat(search(IDRegexp, body))
    : metadataInformation.references;

  const tags = body
    ? metadataInformation.tags.concat(
        search(TagRegexp, body).map((t) => t.replace('#', '')),
      )
    : metadataInformation.tags;

  return {
    ...metadataInformation,
    references,
    tags,
    title,
  };
}

export async function parseZettel(path: string): Promise<ZettelInterface> {
  const zettelFile = await readFile(path);

  try {
    const body = contentToZettel(zettelFile);
    return {
      ...body,
      metadata: {
        path,
        filename: basename(path),
      },
    };
  } catch {
    throw new Error(
      `file-system(parseZettel): unable to parse zettel in path "${path}"`,
    );
  }
}

export function zettelToContent({
  id,
  tags,
  references,
  title,
}: ZettelInterface): string {
  return dedent`
  ${separator}
  ${dump(
    {
      id,
      tags,
      references,
    },
    { indent: 4 },
  ).trimEnd()}
  ${separator}
  # ${title}

  `;
}

export async function newZettel(zettel: ZettelInterface): Promise<void> {
  await writeFile({
    path: zettel.metadata.path,
    filename: zettel.metadata.filename,
    extension: 'md',
    content: zettelToContent(zettel),
  });
}
