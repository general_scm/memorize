import { zettelToContent, contentToZettel } from './zettel';
import { ZettelInterface, ZettelBodyInterface } from '@memorize/types';
import dedent from 'dedent';

describe('Testing Zettel', () => {
  describe('When I want to convert a zettel into markdown content', () => {
    test('It should return a string with yaml metadata and header', () => {
      const zettel: ZettelInterface = {
        id: '20200120160015',
        references: [],
        tags: ['test', 'awesome'],
        title: 'My awesome title',
        metadata: {
          path: '~/zettelkasten/',
          filename: 'test.md',
        },
      };

      const result = dedent`
      ---
      id: '20200120160015'
      tags:
        - test
        - awesome
      references: []
      ---
      # My awesome title
      `;

      expect(zettelToContent(zettel)).toEqual(result);
    });
  });

  describe('When I want to convert a markdown content into a zettel', () => {
    test('It should parse metadata and content', () => {
      const zettel: ZettelBodyInterface = {
        id: '20200120160015',
        references: [],
        tags: ['test', 'awesome'],
        title: 'My awesome title',
      };

      const markdown = dedent`
      ---
      id: '20200120160015'
      tags:
        - test
        - awesome
      references: []
      ---
      # My awesome title
      `;

      expect(contentToZettel(markdown)).toEqual(zettel);
    });

    test('It should parse metadata and content and search for tags', () => {
      const zettel: ZettelBodyInterface = {
        id: '20200120160015',
        references: [],
        tags: ['test', 'awesome', 'new_tag'],
        title: 'My awesome title',
      };

      const markdown = dedent`
      ---
      id: '20200120160015'
      tags:
        - test
        - awesome
      references: []
      ---
      # My awesome title

      This is a sentence with a #new_tag
      `;

      expect(contentToZettel(markdown)).toEqual(zettel);
    });

    test('It should parse metadata and content and search for references', () => {
      const zettel: ZettelBodyInterface = {
        id: '20200120160015',
        references: ['20200120160016', '20200120160017'],
        tags: ['test', 'awesome'],
        title: 'My awesome title',
      };

      const markdown = dedent`
      ---
      id: '20200120160015'
      tags:
        - test
        - awesome
      references: ['20200120160016']
      ---
      # My awesome title

      This is a sentence with a reference to 20200120160017
      `;

      expect(contentToZettel(markdown)).toEqual(zettel);
    });

    describe('And when content is empty', () => {
      test('It should parse metadata and search for references', () => {
        const zettel: ZettelBodyInterface = {
          id: '20200120160015',
          references: ['20200120160016'],
          tags: ['test', 'awesome'],
          title: 'My awesome title',
        };

        const markdown = dedent`
        ---
        id: '20200120160015'
        tags:
          - test
          - awesome
        references: ['20200120160016']
        ---
        `;

        expect(contentToZettel(markdown)).toEqual(zettel);
      });
    });
  });
});
