'use strict'
const baseConfig = require('../../.eslintrc.js')

module.exports = {
   ...baseConfig,
    rules: {
        ...baseConfig.rules,
        'no-console': 'off',
    },
};