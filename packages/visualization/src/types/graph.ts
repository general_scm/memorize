export interface NodeInterface {
  index: number;
  id: string;
  title: string;
  group: number;
}

export interface LinkInterface {
  source: number;
  target: number;
  value: number;
}

export interface GraphInterface {
  nodes: NodeInterface[];
  links: LinkInterface[];
}
