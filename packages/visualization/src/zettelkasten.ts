import {
  ZettelkastenInterface,
  ZettelkastenVisualizationEnum,
  ZettelkastenVisualizationType,
} from '@memorize/types';
import { View, parse, Spec } from 'vega';
// import { zettelkastenToEdgeBundelingDiagram } from './diagrams/edge-bundeling/edge-bundeling.diagram';
import { zettelkastenToArcDiagram } from './diagrams/arc/arc.diagram';
import { zettelkastenToForceDiagram } from './diagrams/force/force.diagram';

export function getZettelkastenVisualization(
  zettelkasten: ZettelkastenInterface,
  type: ZettelkastenVisualizationType,
) {
  let spec: Spec;

  switch (type) {
    case ZettelkastenVisualizationEnum.FORCE:
      spec = zettelkastenToForceDiagram(zettelkasten);
      break;
    case ZettelkastenVisualizationEnum.ARC:
      spec = zettelkastenToArcDiagram(zettelkasten);
      break;
    case ZettelkastenVisualizationEnum.EDGE:
      // spec = zettelkastenToEdgeBundelingDiagram(zettelkasten);
      throw new Error(
        'zettelkasten(getZettelkastenVisualizationSvg): EDGE has not been implemented yet',
      );
    default:
      throw new Error(
        "zettelkasten(getZettelkastenVisualizationSvg): Specified visualizaiton type doesn't estist",
      );
  }

  return new View(parse(spec), { renderer: 'none' });
}

export async function getZettelkastenVisualizationSvg(
  zettelkasten: ZettelkastenInterface,
  type: ZettelkastenVisualizationType,
): Promise<string> {
  return getZettelkastenVisualization(zettelkasten, type).toSVG();
}

export async function getZettelkastenVisualizationCanvas(
  zettelkasten: ZettelkastenInterface,
  type: ZettelkastenVisualizationType,
): Promise<HTMLCanvasElement> {
  return getZettelkastenVisualization(zettelkasten, type).toCanvas();
}
