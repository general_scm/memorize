# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.5.0](https://gitlab.com/memorize_it/memorize/compare/v0.4.0...v0.5.0) (2020-02-24)

**Note:** Version bump only for package @memorize/visualization





# [0.4.0](https://gitlab.com/memorize_it/memorize/compare/v0.3.1...v0.4.0) (2020-02-15)


### Features

* **vscode:** visualize  zettelkasten as svg ([e1228f9](https://gitlab.com/memorize_it/memorize/commit/e1228f9cb7ffc6c88e284277caa8ba4afb10f70c))





## [0.3.1](https://gitlab.com/memorize_it/memorize/compare/v0.3.0...v0.3.1) (2020-02-08)


### Bug Fixes

* **types:** use @memorize/types as dependency to allow use of enums,maps... on runtime ([58c64b0](https://gitlab.com/memorize_it/memorize/commit/58c64b06160c105ac9073e82de8b4d087883fe68))





# [0.3.0](https://gitlab.com/memorize_it/memorize/compare/v0.2.0...v0.3.0) (2020-02-08)


### Features

* **visualization:** force diagram node size depends on times referenced ([6d3e787](https://gitlab.com/memorize_it/memorize/commit/6d3e787f231ace0e808669adcfa825ae2b7cce2a))
* **visualization:** force diagram visualization ([76a74d4](https://gitlab.com/memorize_it/memorize/commit/76a74d446b83118ff85032e50395b1273ef1e8b8))
* **visualization:** generate arc diagram ([693518c](https://gitlab.com/memorize_it/memorize/commit/693518cab16259634a3ceaee44ba607a8d5d7d46))
