export interface ZettelBodyInterface {
  id: string;
  references: string[];
  tags: string[];
  title: string;
}

export interface ZettelInterface extends ZettelBodyInterface {
  metadata: {
    path: string;
    filename: string;
  };
}
