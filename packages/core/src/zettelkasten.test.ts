import { similarTags } from './zettelkasten';
import { ZettelkastenInterface } from '@memorize/types';

describe('Testing Zettelkasten', () => {
  describe('When I want to get similar tags', () => {
    const tags = ['Couse', 'Light', 'Shadow'];
    const zettelkasten: ZettelkastenInterface = {
      meta: {
        updatedAt: 1,
        tags: ['Course', 'Corse'],
      },
      zettels: [
        {
          id: 'id',
          metadata: {
            filename: 'filename',
            path: 'path',
          },
          references: [],
          tags: ['Course'],
          title: 'Title 1',
        },
        {
          id: 'id2',
          metadata: {
            filename: 'filename',
            path: 'path',
          },
          references: [],
          tags: ['Corse'],
          title: 'Title 2',
        },
      ],
    };
    const result = {
      Couse: ['Course', 'Corse'],
      Light: [],
      Shadow: [],
    };

    describe('And I propose a tag Array', () => {
      test('It should return map with the similar options of every tag', () => {
        expect(similarTags(tags, zettelkasten)).toEqual(result);
      });
    });
  });

  describe('When I want to get similar tags and the tag already exists', () => {
    const tags = ['Course', 'Light', 'Shadow'];
    const zettelkasten: ZettelkastenInterface = {
      meta: {
        updatedAt: 1,
        tags: ['Course', 'Corse'],
      },
      zettels: [
        {
          id: 'id',
          metadata: {
            filename: 'filename',
            path: 'path',
          },
          references: [],
          tags: ['Course'],
          title: 'Title 1',
        },
        {
          id: 'id2',
          metadata: {
            filename: 'filename',
            path: 'path',
          },
          references: [],
          tags: ['Corse'],
          title: 'Title 2',
        },
      ],
    };
    const result = {
      Course: [],
      Light: [],
      Shadow: [],
    };

    describe('And I propose a tag Array', () => {
      test('It should return map without suggestions for already existing tag', () => {
        expect(similarTags(tags, zettelkasten)).toEqual(result);
      });
    });
  });
});
