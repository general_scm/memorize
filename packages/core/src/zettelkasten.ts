import { ZettelkastenInterface, ZettelInterface } from '@memorize/types';
import { jaroWinklerDistance } from '@memorize/language';

export function consolidateTags(zettels: ZettelInterface[]) {
  return zettels.reduce((existingTags: string[], { tags }) => {
    return [
      ...existingTags,
      ...tags.filter((item) => existingTags.indexOf(item) < 0),
    ];
  }, []);
}

export function similarTags(
  tagCandidates: string[],
  zettelkasten: ZettelkastenInterface,
  jaroWinklerThreshold = 0.8,
): { [ley: string]: string[] } {
  const existingTags: string[] = zettelkasten.meta.tags;

  return tagCandidates.reduce(
    (tagMap: { [key: string]: string[] }, tag: string) => {
      const matchingTags =
        existingTags.indexOf(tag) >= 0
          ? [] // If tag already exists, no suggestion is proposed
          : existingTags.filter(
              (eTag) => jaroWinklerDistance(tag, eTag) >= jaroWinklerThreshold,
            );
      tagMap[tag] = matchingTags;
      return tagMap;
    },
    {},
  );
}
