export function jaroDistance(s1: string, s2: string): number {
  const s1Length = s1.length;
  const s2Length = s2.length;

  if (s1Length === 0 && s2Length === 0) {
    return 1;
  }

  if (s1Length === 0 || s2Length === 0) {
    return 0;
  }

  if (s1 === s2) {
    return 1;
  }

  const range = Math.floor(Math.max(s1Length, s2Length) / 2.0) - 1;
  const matches1 = new Array(s1Length);
  const matches2 = new Array(s2Length);
  let matches = 0;

  for (let i = 0; i < s1Length; i++) {
    const start = Math.max(0, i - range);
    const end = Math.min(i + range + 1, s2Length);

    for (let k = start; k <= end; k++) {
      if (matches1[i] || matches2[k] || s1[i] !== s2[k]) {
        continue;
      }

      matches++;
      matches1[i] = true;
      matches2[k] = true;
      break;
    }
  }

  if (matches === 0) {
    return 0;
  }

  let k = 0;
  let t = 0;
  for (let i = 0; i < s1Length; i++) {
    if (!matches1[i]) {
      continue;
    }
    while (!matches2[k]) {
      k++;
    }
    if (s1[i] !== s2[k]) {
      t++;
    }

    k++;
  }

  return Number.parseFloat(
    (
      (matches / s1Length + matches / s2Length + (matches - t / 2) / matches) /
      3
    ).toFixed(3),
  );
}
