// Examples from : https://www.census.gov/srd/papers/pdf/rrs2006-02.pdf table 6
export const examples = [
  { s1: 'SHACKLEFORD', s2: 'SHACKELFORD', jaro: 0.97, jaroWinkler: 0.982 },
  { s1: 'DUNNINGHAM', s2: 'CUNNIGHAM', jaro: 0.896, jaroWinkler: 0.896 },
  { s1: 'NICHLESON', s2: 'NICHULSON', jaro: 0.926, jaroWinkler: 0.956 },
  { s1: 'JONES', s2: 'JOHNSON', jaro: 0.79, jaroWinkler: 0.832 },
  { s1: 'MASSEY', s2: 'MASSIE', jaro: 0.889, jaroWinkler: 0.933 },
  { s1: 'ABROMS', s2: 'ABRAMS', jaro: 0.889, jaroWinkler: 0.922 },
  //   { s1: 'HARDIN', s2: 'MARTINEZ', jaro: 0.0, jaroWinkler: 0.0 },
  //   { s1: 'ITMAN', s2: 'SMITH', jaro: 0.0, jaroWinkler: 0.0 },
  { s1: 'JERALDINE', s2: 'GERALDINE', jaro: 0.926, jaroWinkler: 0.926 },
  { s1: 'MARHTA', s2: 'MARTHA', jaro: 0.944, jaroWinkler: 0.961 },
  { s1: 'MICHELLE', s2: 'MICHAEL', jaro: 0.869, jaroWinkler: 0.921 },
  { s1: 'JULIES', s2: 'JULIUS', jaro: 0.889, jaroWinkler: 0.933 },
  { s1: 'TANYA', s2: 'TONYA', jaro: 0.867, jaroWinkler: 0.88 },
  { s1: 'DWAYNE', s2: 'DUANE', jaro: 0.822, jaroWinkler: 0.84 },
  { s1: 'SEAN', s2: 'SUSAN', jaro: 0.783, jaroWinkler: 0.805 },
  { s1: 'JON', s2: 'JOHN', jaro: 0.917, jaroWinkler: 0.934 }, // 0.933 modified to 0.934
  //   { s1: 'JON', s2: 'JAN', jaro: 0.0, jaroWinkler: 0.0 },
];
