# Memorize - Language

Hi there :smile:

This package is created to deal with natural language issues.

Most of the existing repositories addressing this subject are either too large or don't provide types. As these algorithms are well tested and the will hopefully not change anytime soon I have decided to create a module just with the methods required by memorize. 

It provides:

- Jaro distance
- Jaro-winkler distance

Thanks to:

- [@jordanthomas](https://github.com/jordanthomas)/[jaro-winkler](https://github.com/jordanthomas/jaro-winkler)
- [@NaturalNode](https://github.com/NaturalNode)/[natural](https://github.com/NaturalNode/natural)

