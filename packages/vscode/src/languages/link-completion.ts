import {
  CompletionItem,
  SnippetString,
  CompletionItemProvider,
  workspace,
  // eslint-disable-next-line import/no-unresolved
} from 'vscode';
import { loadZettelkasten } from '@memorize/file-system';

export const linkCompletionProvider: CompletionItemProvider = {
  async provideCompletionItems() {
    const rootPath = workspace.rootPath;
    if (!rootPath) {
      return [];
    }
    const zettelkasten = await loadZettelkasten(rootPath);
    return zettelkasten.zettels.map(
      (z): CompletionItem => {
        const idCompletion = new CompletionItem(`${z.id} - ${z.title}`);
        idCompletion.insertText = new SnippetString(z.id);
        return idCompletion;
      },
    );
  },
};
