import {
  workspace,
  DocumentLinkProvider,
  DocumentLink,
  Uri,
  Range,
  TextLine,
  // eslint-disable-next-line import/no-unresolved
} from 'vscode';
import { IDRegexp } from '@memorize/core';
import { loadZettelkasten } from '@memorize/file-system';
import { ZettelInterface } from '@memorize/types';

function buildLink(
  line: TextLine,
  lineIndex: number,
  { id, metadata, title: tooltip }: ZettelInterface,
): DocumentLink {
  const startCharacter = line.text.indexOf(id);
  const endCaracter = startCharacter + id.length;
  const range = new Range(lineIndex, startCharacter, lineIndex, endCaracter);
  const target = Uri.parse(metadata.path);

  return {
    range,
    target,
    tooltip,
  };
}

export const linkDocumentProvider: DocumentLinkProvider = {
  async provideDocumentLinks(document): Promise<DocumentLink[]> {
    const rootPath = workspace.rootPath;
    if (!rootPath) {
      return [];
    }

    const links: DocumentLink[] = [];
    const zettelkasten = await loadZettelkasten(rootPath);

    for (let lineIndex = 0; lineIndex < document.lineCount; lineIndex++) {
      const line = document.lineAt(lineIndex);
      const matches = line.text.match(IDRegexp);

      if (matches) {
        matches.forEach((id) => {
          const zettel = zettelkasten.zettels.find((z) => z.id === id);
          if (zettel) {
            links.push(buildLink(line, lineIndex, zettel));
          }
        });
      }
    }

    return links;
  },
};
