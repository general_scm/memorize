// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import {
  ExtensionContext,
  commands,
  WebviewPanel,
  languages,
  // eslint-disable-next-line import/no-unresolved
} from 'vscode';
import { generateId, createNewZettel } from './commands/zettel';
import { visualizeZettelkasten } from './commands/visualization';
import { linkCompletionProvider } from './languages/link-completion';
import { tagCompletionProvider } from './languages/tag-completion';
import { linkDocumentProvider } from './languages/link-document';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: ExtensionContext): void {
  // Use the console to output diagnostic information (console.log) and errors (console.error)
  // This line of code will only be executed once when your extension is activated
  console.log('Congratulations, your extension "Memorize" is now active!');

  const generateIdCmd = commands.registerCommand(
    'extension.generateId',
    generateId,
  );
  context.subscriptions.push(generateIdCmd);

  const currentPanel: WebviewPanel | undefined = undefined;
  const visualizeCmd = commands.registerCommand('extension.visualize', () =>
    visualizeZettelkasten(context, currentPanel),
  );
  context.subscriptions.push(visualizeCmd);

  const newZettelCmd = commands.registerCommand(
    'extension.newZettel',
    createNewZettel,
  );
  context.subscriptions.push(newZettelCmd);

  const linkAutocompletion = languages.registerCompletionItemProvider(
    'markdown',
    linkCompletionProvider,
    '[',
  );
  context.subscriptions.push(linkAutocompletion);

  const tagAutocompletion = languages.registerCompletionItemProvider(
    'markdown',
    tagCompletionProvider,
    '#',
  );
  context.subscriptions.push(tagAutocompletion);

  const zettelLink = languages.registerDocumentLinkProvider(
    'markdown',
    linkDocumentProvider,
  );
  context.subscriptions.push(zettelLink);
}

// this method is called when your extension is deactivated
// export function deactivate() {}
