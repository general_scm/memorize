import {
  window,
  // eslint-disable-next-line import/no-unresolved
} from 'vscode';

export async function showInputBox(
  prompt: string,
  placeHolder: string,
  defaultValue?: string,
): Promise<string | undefined> {
  const input = await window.showInputBox({
    prompt,
    placeHolder,
    value: defaultValue,
  });

  return input;
}
