# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.5.0](https://gitlab.com/memorize_it/memorize/compare/v0.4.0...v0.5.0) (2020-02-24)


### Features

* **vscode:** add new zettel ([9e6514d](https://gitlab.com/memorize_it/memorize/commit/9e6514d7454334300648d95a8db986fce2d7c9fc))





# [0.4.0](https://gitlab.com/memorize_it/memorize/compare/v0.3.1...v0.4.0) (2020-02-15)


### Features

* **vscode:** add tag and link completion ([57bb437](https://gitlab.com/memorize_it/memorize/commit/57bb437b615d0627aefff2716817b0af945fff2a))
* **vscode:** bootstrap vscode extension with generateId ([29f7e22](https://gitlab.com/memorize_it/memorize/commit/29f7e222c082a740f4bc74ec1ba85344d5630f0f))
* **vscode:** follow zettel id link ([d8d6235](https://gitlab.com/memorize_it/memorize/commit/d8d623582e9da28e05641efde57a4ea22cfe0c49))
* **vscode:** visualize  zettelkasten as svg ([e1228f9](https://gitlab.com/memorize_it/memorize/commit/e1228f9cb7ffc6c88e284277caa8ba4afb10f70c))
