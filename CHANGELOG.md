# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.5.0](https://gitlab.com/memorize_it/memorize/compare/v0.4.0...v0.5.0) (2020-02-24)


### Features

* **vscode:** add new zettel ([9e6514d](https://gitlab.com/memorize_it/memorize/commit/9e6514d7454334300648d95a8db986fce2d7c9fc))





# [0.4.0](https://gitlab.com/memorize_it/memorize/compare/v0.3.1...v0.4.0) (2020-02-15)


### Features

* **core:** add method to zettelkasten to consolidate tags ([9ad3e12](https://gitlab.com/memorize_it/memorize/commit/9ad3e129f9502ff7baa7d0920f53ca5b5e1f8ef3))
* **file-system:** addtags to metadata when loading zettelkasten ([33db0c6](https://gitlab.com/memorize_it/memorize/commit/33db0c62cc6a69d9fb0c310959ca14ac03e0fa4b))
* **types:** add tagsto zettelkasten metadata ([29b5e18](https://gitlab.com/memorize_it/memorize/commit/29b5e18162aea533b2d03e372051b047cf29ea6c))
* **vscode:** add tag and link completion ([57bb437](https://gitlab.com/memorize_it/memorize/commit/57bb437b615d0627aefff2716817b0af945fff2a))
* **vscode:** bootstrap vscode extension with generateId ([29f7e22](https://gitlab.com/memorize_it/memorize/commit/29f7e222c082a740f4bc74ec1ba85344d5630f0f))
* **vscode:** follow zettel id link ([d8d6235](https://gitlab.com/memorize_it/memorize/commit/d8d623582e9da28e05641efde57a4ea22cfe0c49))
* **vscode:** visualize  zettelkasten as svg ([e1228f9](https://gitlab.com/memorize_it/memorize/commit/e1228f9cb7ffc6c88e284277caa8ba4afb10f70c))





## [0.3.1](https://gitlab.com/memorize_it/memorize/compare/v0.3.0...v0.3.1) (2020-02-08)


### Bug Fixes

* **types:** use @memorize/types as dependency to allow use of enums,maps... on runtime ([58c64b0](https://gitlab.com/memorize_it/memorize/commit/58c64b06160c105ac9073e82de8b4d087883fe68))





# [0.3.0](https://gitlab.com/memorize_it/memorize/compare/v0.2.0...v0.3.0) (2020-02-08)


### Bug Fixes

* **core:** move regeps from types to core. They are used on the runtime !!! XD ([c613128](https://gitlab.com/memorize_it/memorize/commit/c6131288b5c50d6962e8c59ee6f4df03a4d4c60d))


### Features

* **cli:** Allow to generate zettelkasten svg visualization and update aliases ([992b5a8](https://gitlab.com/memorize_it/memorize/commit/992b5a8b9e7586957530998592c9741c0540750c))
* **cli:** ask for diagram type when creating visualization svg ([cb97561](https://gitlab.com/memorize_it/memorize/commit/cb975614af8e29c2b353859afb63bde9bc37ff65))
* **cli:** make tag suggestion when similar tags  are introduced ([1c7a2d5](https://gitlab.com/memorize_it/memorize/commit/1c7a2d594a732bb46d185c6bcb0866eddac472ff))
* **core:** allow to search for similar  tags already exisitng ([984c478](https://gitlab.com/memorize_it/memorize/commit/984c47881fa4a32499fcb84f31bc07d44e03d7d3))
* **core:** wHen parsing similartags don't make any suggestion if tag already exists ([ae843b9](https://gitlab.com/memorize_it/memorize/commit/ae843b97e913167e99e14dc5d8a6cf853d14de16))
* **file-system:** add diagram type to zettelkasten visualization ([ca5e736](https://gitlab.com/memorize_it/memorize/commit/ca5e736a0ee61b587896c9c61aa9e74a8e260ca2))
* **file-system:** create svg file with zettelkasten visualization ([3a13c93](https://gitlab.com/memorize_it/memorize/commit/3a13c9349073d444eac024fcd221b19adf56730c))
* **language:** jaro and jaro-winkler distances ([f2dbfd7](https://gitlab.com/memorize_it/memorize/commit/f2dbfd7080022142ba96d14f6c3948d4310a5095))
* **types:** define diagram types ([a67c142](https://gitlab.com/memorize_it/memorize/commit/a67c142bab52a6a1c09d6e3445dbd8aba134b076))
* **visualization:** force diagram node size depends on times referenced ([6d3e787](https://gitlab.com/memorize_it/memorize/commit/6d3e787f231ace0e808669adcfa825ae2b7cce2a))
* **visualization:** force diagram visualization ([76a74d4](https://gitlab.com/memorize_it/memorize/commit/76a74d446b83118ff85032e50395b1273ef1e8b8))
* **visualization:** generate arc diagram ([693518c](https://gitlab.com/memorize_it/memorize/commit/693518cab16259634a3ceaee44ba607a8d5d7d46))





# [0.2.0](https://gitlab.com/memorize_it/memorize/compare/v0.1.1...v0.2.0) (2020-01-20)


### Features

* **file-system:** add basic file-system operations ([123c5f0](https://gitlab.com/memorize_it/memorize/commit/123c5f091c74352a7f80199abd185863021f3ef2))
* **file-system:** parse zettel from file and zettel to file ([e24a35d](https://gitlab.com/memorize_it/memorize/commit/e24a35df0b1160480021f1e0620b72bf8b85e476))
* **graph:** zettelkasten to graph funtion ([41df447](https://gitlab.com/memorize_it/memorize/commit/41df447ce26cd8d4220bd5815a40c1054e21edf9))
* **types:** add new types module ([ea1f418](https://gitlab.com/memorize_it/memorize/commit/ea1f418e4bba629ca5172f7b1d32ed42bafb7d59))
* **utils:** add new utils module ([ebb2bbb](https://gitlab.com/memorize_it/memorize/commit/ebb2bbb6403433c697c1c3487e0bf3838d21624f))
* **zettelkasten:** initialize a zettelkasten on target directory. ([b8f810c](https://gitlab.com/memorize_it/memorize/commit/b8f810cdf6d1f60cce4a41f846ac906ff53dd0c2))
* **zettel:** add sanitize  text method ([fb0cfcf](https://gitlab.com/memorize_it/memorize/commit/fb0cfcf7c35a342d4494c24adf89da79038217ec))
* **zettel:** add zettel from the cli ([012c4bd](https://gitlab.com/memorize_it/memorize/commit/012c4bd66439118af178c445be208358c9a7696c))
* **zettel:** differentiate zettelbody and metadata ([dbd07a4](https://gitlab.com/memorize_it/memorize/commit/dbd07a4f76057e1aa3b21ab53dc5c49c9984adae))





## [0.1.1](https://gitlab.com/memorize_it/memorize/compare/v0.1.0...v0.1.1) (2020-01-18)

**Note:** Version bump only for package memorize





# 0.1.0 (2020-01-18)


### Features

* **cli:** add cli with create id feature ([02fa676](https://gitlab.com/memorize_it/memorize/commit/02fa67638c6745cea73d2026e187b9b9980102bc))
* **core:** create id from date ([85d59c2](https://gitlab.com/memorize_it/memorize/commit/85d59c2a74173d1c3ed224c9e9ee1b7295d3ed4c))
