# Developper readme

## Required libs

``` bash
npm set registry https://registry.npmjs.org 
npm i -g yarn lerna verdaccio
```

## Monorepo and setup

[Monorepo: babel + ts + lerna starter](https://github.com/serhii-havrylenko/monorepo-babel-ts-lerna-starter)
